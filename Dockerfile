FROM caddy:2-builder AS builder
RUN xcaddy build \
    --with github.com/pteich/caddy-tlsconsul \
    --with github.com/greenpau/caddy-auth-portal \
    --with github.com/greenpau/caddy-authorize \
    --with github.com/porech/caddy-maxmind-geolocation \
    --with github.com/lindenlab/caddy-s3-proxy

FROM caddy:2
COPY --from=builder /usr/bin/caddy /usr/bin/caddy
